# coding: utf-8

from os import path
import datetime
import pandas as pd

class DatetimeEventStore:

    def __init__(self):
        pass

    def store_event(self, year, month, day, event):
        '''permet de stocker un évènement en l’associant à un ​datetime.datetime'''

        at = datetime.datetime(year, int(month), int(day)).timestamp()
        # création d'un fichier save_events.csv dans le path du script
        with open('save_events.csv', 'a') as fichier:
            a_sauver = "{},{}".format(at, event)
            fichier.write(a_sauver+"\n")
        print("\nL'événement {}({}-{}-{}) a été stocké\n".format(event, year, month, day))

    def get_events(self, year_st, month_st, day_st, year_en, month_en, day_en):
        '''permet de récupérer les évènements associés aux datetimes appartenant à la période spécifiée en paramètre'''

        # mise en forme des dates pour les bornes de l'intervalle
        start_ts = datetime.datetime(int(year_st), int(month_st), int(day_st)).timestamp()
        end_ts = datetime.datetime(int(year_en), int(month_en), int(day_en)).timestamp()
        if path.exists('save_events.csv'):
            events = pd.read_csv('save_events.csv', header=None)
            # transformation du tableau en filtrant sur les bornes données par l'utilisateur
            dt = events[(events[0] >= start_ts) & (events[0] <= end_ts)]
            # désactivation des warnings
            pd.options.mode.chained_assignment = None
            # affichage du tableau des données filtrées
            for x, y in dt.iterrows():
                dt[0][x] = datetime.datetime.fromtimestamp(dt[0][x]).date()
            dt.columns=['DATE', 'EVENEMENTS']
            print("\n",dt,"\n")
        else:
            print("Commencez par rentrer un premier enregistrement svp\n")
