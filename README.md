# Install
## Dependencies
<code>
pip install pandas
</code>

# Use
<code>
python main.py
</code>

# Subject
A python package allowing to record and consult events linked to dates.

<p>This package is intended for use and maintenance by other developers on the team. the
package must implement a ​DatetimeEventStore​ class having two methods
main:</p>
<ul>
<li>DatetimeEventStore.store_event(at, event)​​ allowing to store a
event by associating it with a ​datetime.datetime​​.</li>
<li>DatetimeEventStore.get_events(start, end)​​ to retrieve the
events associated with datetimes belonging to the period specified in parameter.</li>
</ul>
