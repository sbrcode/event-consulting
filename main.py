# coding: utf-8

from os import system
from storage.datetimeeventrecorder import DatetimeEventStore

store = DatetimeEventStore()
choix = None
system('clear')

# ici on peut ajouter des fonctionnalités dans le menu
main_menu = [
    ('0', "Quitter", 'exit()'),
    ('1', "Enregistrer un nouvel événement", 'store.store_event(var1, var2, var3, var4)', 'Année : ', 'Mois : ', 'Jour : ', 'Evénement : '),
    ('2', "Consulter des événements", 'store.get_events(var1, var2, var3, var4, var5, var6)', 'Année de Début : ', 'Mois de Début : ', 'Jour de Début : ',\
        'Année de Fin : ', 'Mois de Fin : ', 'Jour de Fin : ')
            ]

def affiche_menu(menu):
    '''affichage standard du menu'''

    for ligne in menu:
        print("{} - {}".format(ligne[0], ligne[1]))
    choix = input("Quel choix svp: ")
    return choix

def exec_menu(choix, menu):
    '''permet d'exécuter les fonctionnalités des méthodes proposées, dépend du nb de paramètres'''
    
    for x in range(len(menu)):
        if choix == menu[x][0]:
            if len(menu[x]) == 7:
                var1 = int(input(menu[x][3]))
                var2 = input(menu[x][4])
                var3 = input(menu[x][5])
                var4 = input(menu[x][6])
            elif len(menu[x]) == 9:
                var1 = input(menu[x][3])
                var2 = input(menu[x][4])
                var3 = input(menu[x][5])
                var4 = input(menu[x][6])
                var5 = input(menu[x][7])
                var6 = input(menu[x][8])
            exec(menu[x][2])
    
print("Bienvenue dans votre gestionnaire d'événements :")
if __name__ == "__main__":
    while choix != 0:
        exec_menu(affiche_menu(main_menu), main_menu)
